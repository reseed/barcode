##Barcode module

This module provides a barcode in Rebecca web app.

##Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Add

```
{
  "type": "git",
  "url": "git@bitbucket.org:reseed/barcode.git"
},
```

to the `repositories` section of your `composer.json` and

```
"reseed/barcode": "*",
```

to the `require` section of your `composer.json` file.

##Configuration

Add module to backend or common config file

```
'modules' => [
    'barcode' => [
        'class' => 'reseed\barcode\Module',
    ],
]
```

## How to use:

In your view you can use barcode widget

```
<?= \reseed\barcode\widgets\Barcode::widget([
    'text' =>'1111-1111-1111',
    'size' => 50,
    'codeType' => 'codabar',    //  code128, code39, code25, codabar
    'orientation' => 'horizontal', //   horizontal|vertical
]);
```